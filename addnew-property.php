<?php include 'header.php'; ?>
<div class="container add_property">
	<div class="property-title property-add">
		<h3>Property Title</h3>
		<div class="col-md-12 form-area">
			<label>Title</label>
			<input type="text" name="" placeholder="Property Title">
		</div>
	</div>
	<div class="property-location property-add">
		<h3>Property Location</h3>
		<div class="col-md-4 form-area">
			<label>Address</label>
			<input type="text" name="" placeholder="No:51 Usa">
		</div>
		<div class="col-md-4 form-area">
			<label>Neighbourhood</label>
			<input type="text" name="" placeholder="Cambodia">
		</div>
		<div class="col-md-4 form-area">
			<label>City</label>
			<input type="text" name="" placeholder="Phnom Penh">
		</div>
		<div class="col-md-4 form-area">
			<label>Postal code/ Zip</label>
			<input type="text" name="" placeholder="600 099">
		</div>
		<div class="col-md-4 form-area">
			<label>Country / State</label>
			<input type="text" name="" placeholder="Battambang">
		</div>
		<div class="col-md-4 form-area">
			<label>Country</label>
			<input type="text" name="" placeholder="Malasiya">
		</div>
	</div>
	<div class="google-map property-add">
		<h3>Google Map</h3>
		<div class="col-md-9 show-map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d250151.15120226325!2d104.75009733980517!3d11.579666940924923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109513dc76a6be3%3A0x9c010ee85ab525bb!2sPhnom+Penh!5e0!3m2!1sen!2skh!4v1555229597158!5m2!1sen!2skh" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="col-md-3 map-latitude">
			<div class="form-area">
				<label>Google Map Latitude</label>
				<input type="text" name="" placeholder="40.1725689">
			</div>
			<div class="form-area">
				<label>Google Map Longitude</label>
				<input type="text" name="" placeholder="74.291295">
			</div>
			<div class="form-area">
				<input type="submit" name="" value="Place the pin above the Address">
			</div>
		</div>
	</div>
	<div class="property-type property-add">
		<h3>Property Type</h3>
		<div class="col-md-4 form-area">
			<label>Type</label>
			<select class="form-control" id="prop_type">
		      <option>Rent</option>
		      <option>Sale</option>
		      <option>Lease</option>
		    </select>
		</div>
		<div class="col-md-4 form-area">
			<label>Status</label>
			<select class="form-control" id="prop_status">
		      <option>Available</option>
		      <option>Not Available</option>
		    </select>
		</div>
		<div class="col-md-4 form-area">
			<label>Label</label>
			<select class="form-control" id="prop_label">
		      <option>Click</option>
		      <option>Share</option>
		    </select>
		</div>
	</div>
	<div class="property-price property-add">
		<h3>Property Price</h3>
		<div class="col-md-4 form-area">
			<label>Sale or Rent Price</label>
			<input type="text" name="" placeholder="$10,000,000">
		</div>
		<div class="col-md-4 form-area">
			<label>After Price per Month</label>
			<input type="text" name="" placeholder="$20,000,000">
		</div>
	</div>
	<div class="property-features property-add">
		<h3>Property Features</h3>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Air Conditioning</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Central Heating</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Electric Range</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Fire Alarm</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Gym</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Home Theater</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Laundry</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Laundry Room</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Marble Floors</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Microwave</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Washer</label>
		</div>
		<div class="col-md-3 form-area">
			<label><input type="checkbox" name="">Wifi	</label>
		</div>
	</div>
	<div class="property-details property-add">
		<h3>Property Details</h3>
		<div class="col-md-3 form-area">
			<label>Area Size (Sqft)</label>
			<input type="text" name="" placeholder="3,175">
		</div>
		<div class="col-md-3 form-area">
			<label>Land Area Size (Sqft)</label>
			<input type="text" name="" placeholder="2,250">
		</div>
		<div class="col-md-3 form-area">
			<label>Bedrooms</label>
			<input type="text" name="" placeholder="2">
		</div>
		<div class="col-md-3 form-area">
			<label>Bathrooms</label>
			<input type="text" name="" placeholder="3">
		</div>
		<div class="col-md-3 form-area">
			<label>Garages</label>
			<input type="text" name="" placeholder="1">
		</div>
		<div class="col-md-3 form-area">
			<label>Garages Size (Sqft)</label>
			<input type="text" name="" placeholder="1,270">
		</div>
		<div class="col-md-3 form-area">
			<label>Year built</label>
			<input type="text" name="" placeholder="2015">
		</div>
		<div class="col-md-3 form-area">
			<label>Video URL</label>
			<input type="text" name="" placeholder="Youtube">
		</div>
	</div>
	<div class="property-media property-add">
		<h3>Image Upload</h3>
		<label for="upload_picture">
			<i class="icon-upload"></i>
			<h3>Upload Images Here</h3>
			<input type="file" name="" id="upload_picture">
		</label>
	</div>
	<div class="property-media property-add">
		<h3>Video Upload</h3>
		<label for="upload_video">
			<i class="icon-upload"></i>
			<h3>Upload Video Here</h3>
			<input type="file" name="" id="upload_video">
		</label>
	</div>
	<div class="property-media property-add">
		<h3>Plan Upload</h3>
		<label for="upload_plan">
			<i class="icon-upload"></i>
			<h3>Upload Plans Here</h3>
			<input type="file" name="" id="upload_plan">
		</label>
	</div>
	<div class="property-submit">
		<input type="submit" name="" value="Submit Property">
	</div>
</div>

<?php include 'footer.php'; ?>