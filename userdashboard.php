<?php include 'header.php'; ?>
<div class="property-userdashboard">
	<div class="container">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
			<li class="nav-item active">
				<a class="nav-link" data-toggle="tab" href="#my_profile" role="tab" aria-selected="true">My Profile</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#my_properties" role="tab" aria-selected="false">My Properties</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#my_invoices" role="tab" aria-selected="false">My Invoices</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#my_fav" role="tab" aria-selected="false">My Favorites</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#my_search" role="tab" aria-selected="false">My Saved Search</a>
			</li>
			<li class="pull-right">
				<a href="#">Add New Property</a>
			</li>
		</ul>
		<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade active in" id="my_profile" role="tabpanel">
				<?php include 'includes/dashboard-myprofile.php'; ?>
			</div>
			<div class="tab-pane fade" id="my_properties" role="tabpanel">
				<?php include 'includes/dashboard-myproperty.php'; ?>
			</div>
			<div class="tab-pane fade" id="my_invoices" role="tabpanel">
				<h3>My Invoices</h3>
			</div>
			<div class="tab-pane fade" id="my_fav" role="tabpanel">
				<h3>My Favorites</h3>
			</div>
			<div class="tab-pane fade" id="my_search" role="tabpanel">
				<h3>My Saved Search</h3>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>