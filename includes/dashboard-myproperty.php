<div class="property-types">
	<ul class="nav nav-tabs" id="myTabs" role="tablist">
		<li class="nav-item active">
			<a class="nav-link" data-toggle="tab" href="#all_property" role="tab" aria-selected="true">All(1)</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#approved_property" role="tab" aria-selected="false">Approved(1)</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#pending_property" role="tab" aria-selected="false">Pending(1)</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#expired_property" role="tab" aria-selected="false">Expired(1)</a>
		</li>
		<li class="pull-right">
			<label>
				<input type="text" name="" placeholder="Search.....">
				<button type="submit">Search</button>
			</label>
		</li>
	</ul>
	<div class="tab-content" id="myTabContents">
		<div class="tab-pane fade active in" id="all_property" role="tabpanel">
			<table>
				<thead>
					<tr>
						<th>Picture</th>
						<th class="heading-title">Title</th>
						<th>Date Posted</th>
						<th>Status</th>
						<th>Featured</th>
					</tr>
				</thead>
				<tbody>
					<?php for($i = 0; $i < 10; $i ++): ?>
					<tr>
						<td>
							<figure>
								<img src="dist/images/real-estate.jpg">
							</figure>
						</td>
						<td>
							<div class="property-title">
								<h3>House for rent in Barley Hill</h3>
								<h4><i class="icon-map"></i> 4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn,Phnom Penh</h4>
								<a href="#">Edit</a><a href="#">Delete</a><a href="#">Expired</a>
							</div>
						</td>
						<td>February 16, 2017</td>
						<td>Publish</td>
						<td>2 Days Ago</td>
					</tr>
					<?php endfor; ?> 
				</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="approved_property" role="tabpanel">
			<table>
				<thead>
					<tr>
						<th>Picture</th>
						<th class="heading-title">Title</th>
						<th>Date Posted</th>
						<th>Status</th>
						<th>Featured</th>
					</tr>
				</thead>
				<tbody>
					<?php for($i = 0; $i < 5; $i ++): ?>
					<tr>
						<td>
							<figure>
								<img src="dist/images/real-estate.jpg">
							</figure>
						</td>
						<td>
							<div class="property-title">
								<h3>House for rent in Barley Hill</h3>
								<h4><i class="icon-map"></i> 4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn,Phnom Penh</h4>
								<a href="#">Edit</a><a href="#">Delete</a><a href="#">Expired</a>
							</div>
						</td>
						<td>February 16, 2017</td>
						<td>Publish</td>
						<td>2 Days Ago</td>
					</tr>
					<?php endfor; ?> 
				</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="pending_property" role="tabpanel">
			<table>
				<thead>
					<tr>
						<th>Picture</th>
						<th class="heading-title">Title</th>
						<th>Date Posted</th>
						<th>Status</th>
						<th>Featured</th>
					</tr>
				</thead>
				<tbody>
					<?php for($i = 0; $i < 3; $i ++): ?>
					<tr>
						<td>
							<figure>
								<img src="dist/images/real-estate.jpg">
							</figure>
						</td>
						<td>
							<div class="property-title">
								<h3>House for rent in Barley Hill</h3>
								<h4><i class="icon-map"></i> 4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn,Phnom Penh</h4>
								<a href="#">Edit</a><a href="#">Delete</a><a href="#">Expired</a>
							</div>
						</td>
						<td>February 16, 2017</td>
						<td>Publish</td>
						<td>2 Days Ago</td>
					</tr>
					<?php endfor; ?> 
				</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="expired_property" role="tabpanel">
			<table>
				<thead>
					<tr>
						<th>Picture</th>
						<th class="heading-title">Title</th>
						<th>Date Posted</th>
						<th>Status</th>
						<th>Featured</th>
					</tr>
				</thead>
				<tbody>
					<?php for($i = 0; $i < 1; $i ++): ?>
					<tr>
						<td>
							<figure>
								<img src="dist/images/real-estate.jpg">
							</figure>
						</td>
						<td>
							<div class="property-title">
								<h3>House for rent in Barley Hill</h3>
								<h4><i class="icon-map"></i> 4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn,Phnom Penh</h4>
								<a href="#">Edit</a><a href="#">Delete</a><a href="#">Expired</a>
							</div>
						</td>
						<td>February 16, 2017</td>
						<td>Publish</td>
						<td>2 Days Ago</td>
					</tr>
					<?php endfor; ?> 
				</tbody>
			</table>
		</div>
	</div>
</div>