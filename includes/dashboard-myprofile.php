<div class="profile-area">
	<ul class="nav nav-tabs col-md-3" id="mine_profiles" role="tablist">
		<h3>Profile Setting</h3>
		<li class="nav-item active">
			<a class="nav-link" data-toggle="tab" href="#all_profile" role="tab" aria-selected="true">Profile</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#profile_password" role="tab" aria-selected="false">Password</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#profile_security" role="tab" aria-selected="false">Security</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#profile_deativate" role="tab" aria-selected="false">Account Deactivate</a>
		</li>
	</ul>
	<div class="tab-content col-md-9" id="myTabprofile">
		<div class="tab-pane fade active in" id="all_profile" role="tabpanel">
			<div class="epic-profile">
				<h3>Profile</h3>
				<div class="back-fixed">
					<figure><img src="dist/images/banner-profile.jpg"></figure>
				</div>
				<div class="profile_pic">
					<figure><img src="dist/images/profile.jpg"></figure>
				</div>
				<div class="update-profile">
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="First Name">
					</div>
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="Last Name">
					</div>
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="Email">
					</div>
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="Phone Number">
					</div>
					<div class="form-submit">
						<input type="submit" class="form-cancel" name="" value="Cancel">
						<input type="submit" class="form-save" name="" value="Save">
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="profile_password" role="tabpanel">
			<div class="epic-profile">
				<h3>Password</h3>
				<div class="update-password">
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="Current Password">
					</div>
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="New Password">
					</div>
					<div class="form-area col-md-6">
						<input type="text" name="" placeholder="Confirm New Password">
					</div>
					<div class="form-submit">
						<input type="submit" class="form-cancel" name="" value="Cancel">
						<input type="submit" class="form-save" name="" value="Save">
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="profile_security" role="tabpanel">
			
		</div>
		<div class="tab-pane fade" id="profile_deativate" role="tabpanel">
			<div class="epic-profile">
				<h3>Account Deactivate</h3>
					
			</div>
		</div>
	</div>
</div>