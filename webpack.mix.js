let mix = require('laravel-mix');
/*mix.webpackConfig({ devtool: 'inline-source-map' });
mix.options({
    processCssUrls: false
});*/
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

/*mix.sass(
	'assets/styles/style.scss','dist/styles/style.css'
).sourceMaps();*/

mix.styles([
	'assets/styles/style.scss',
	'node_modules/bootstrap/dist/css/bootstrap.min.css',
	'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
	//'node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css',
	'node_modules/bootstrap-select/dist/css/bootstrap-select.min.css',
	'node_modules/animate.css/animate.min.css',
	], 'dist/styles/vendor.css', 'dist/styles').sourceMaps();
mix.sass('assets/styles/style.scss', 'dist/styles/style.css').sourceMaps();

// mix.scripts([
// 	'node_modules/jquery/dist/jquery.min.js',
// 	'node_modules/jquery-validation/dist/jquery.validate.min.js',
// 	'node_modules/bootstrap/dist/js/bootstrap.min.js',
// 	'assets/plugins/meanMenu/jquery.meanmenu.min.js',
// 	], 'dist/scripts/plugins/vendor.js', 'dist/scripts/');

// mix.scripts([
// 	'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
// 	'assets/scripts/dropzone.js',
// 	'assets/scripts/skrollr.min.js',
// 	'assets/scripts/plugins/jquery.waterwheelCarousel.js',
// 	'assets/scripts/plugins/owl.carousel.min.js',
// 	'node_modules/cropper/dist/cropper.min.js',
// 	'node_modules/jquery-easing/dist/jquery.easing.1.3.umd.min.js',
// 	'node_modules/bootstrap-tagsinput/bootstrap-tagsinput.min.js'
// 	], 'dist/scripts/vendor-footer.js', 'dist/scripts/');
// mix.scripts([
// 	'assets/scripts/script.js',
// 	'assets/scripts/form-control.js',
// 	'assets/scripts/main.js'
// 	], 'dist/scripts/app.js', 'dist/scripts');
mix.webpackConfig({ devtool: 'inline-source-map' });
mix.options({
    processCssUrls: false
});