<?php include 'header.php'; ?>
<div class="adding-banner">
	<div class="banner-section container listing-area">
		<div class="search-result">
			<select class="form-control" id="select_type">
				<option>Buy</option>
				<option>Rent</option>
				<option>Short Term Rent</option>
				<option>Commercial Short Term Rent</option>
				<option>Commercial Buy</option>
				<option>Commercial Rent</option>
		    </select>
		    <input type="text" name="" placeholder="Areas, Cities and buildings..">
		    <button type="button">Search</button>
		</div>
		<div class="filter-search">
			<div class="collapse" id="adv_search">
				<?php for($i = 0; $i < 10; $i ++): ?>
			    <select class="form-control" id="prop_type">
					<option>Buy</option>
					<option>Rent</option>
					<option>Short Term Rent</option>
					<option>Commercial Short Term Rent</option>
					<option>Commercial Buy</option>
					<option>Commercial Rent</option>
			    </select>
			    <?php endfor; ?> 
			</div>
			<button class="btn" type="button" data-toggle="collapse" data-target="#adv_search" aria-expanded="false" aria-controls="collapseExample">+ Advance Search</button>
			<button>Clear Filter</button>	
		</div>
	</div>
</div>
<div class="detail-collection container">
	<div class="col-md-8 gallery-view">
		<div class="estate-name">
			<div class="pull-right">
				<h4>USD 93,330,000</h4>
				<button class="btn"><i class="icon-heart"></i></button>
				<div class="dropdown">
				  <button class="btn" type="button" id="sort_resu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="icon-share2"></i>
				  </button>
				  <div class="dropdown-menu" aria-labelledby="sort_resu">
				    <a href="#"><i class="icon-facebook"></i></a>
					<a href="#"><i class="icon-twitter2"></i></a>
					<a href="#"><i class="icon-instagram2"></i></a>
					<a href="#"><i class="icon-google"></i></a>
				  </div>
				</div>
			</div>
			<h3>Dorchester Sky Collection Duplex Penthouse</h3>
			<h5>Reference No : OBG-S-15236 | ONE PALM, The Palm Jumeirah, Dubai</h5>
			<h5>Penthouse for Sale  <span><i class="icon-box"></i>5</span> <span><i class="icon-box"></i>5</span> <span><i class="icon-box"></i>21294 sqft</span></h5>
		</div>
		<div class="gall-maps">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			  <li class="nav-item active">
			    <a class="nav-link" id="gallery-tab" data-toggle="tab" href="#gall" role="tab" aria-selected="true">Gallery</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="map-tab" data-toggle="tab" href="#maps" role="tab" aria-selected="false">Map and Nearby</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="local-tab" data-toggle="tab" href="#local_info" role="tab" aria-selected="false">Local Info</a>
			  </li>
			</ul>
			<div class="tab-content" id="myTabContent">
			  <div class="tab-pane fade active in" id="gall" role="tabpanel">
			  	<div class="container-fluid owl-carousel">
					<?php for($i = 0; $i < 5; $i ++): ?>
				    <div class="item">
				        <img src="dist/images/gall1.jpg">
				        <div class="narrow-content-box">
				        </div>
				    </div>
				    <div class="item">
				         <img src="dist/images/gall2.jpg">
				         <div class="narrow-content-box">
				        </div>
				    </div>
				    <?php endfor; ?> 
				</div>
			  </div>
			  <div class="tab-pane fade" id="maps" role="tabpanel">
			  	<div class="col-md-12 select-map">
			  		<div class="select-place col-md-9">
				  		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.987643314419!2d104.84228751530667!3d11.552743291797572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31094fd553400b85%3A0xd5336e32f60085ec!2sPhnom+Penh+International+Airport!5e0!3m2!1sen!2skh!4v1549337440828" width="100%" height="520" frameborder="0" style="border:0" allowfullscreen></iframe>
				  	</div>
				  	<div class="checks-id col-md-3">
				  		<label><input type="checkbox" name="">Temple</label>
				  		<label><input type="checkbox" name="">Hospital</label>
				  		<label><input type="checkbox" name="">School</label>
				  		<label><input type="checkbox" name="">Gym</label>
				  		<label><input type="checkbox" name="">Bank</label>
				  		<label><input type="checkbox" name="">Shopping Mall</label>
				  		<label><input type="checkbox" name="">Park</label>
				  		<label><input type="checkbox" name="">Gas Station</label>
				  		<label><input type="checkbox" name="">Restaurent</label>
				  	</div>
			  	</div>
			  </div>
			  <div class="tab-pane fade" id="local_info" role="tabpanel">
			  	<img src="dist/images/gall2.jpg">
			  </div>
			</div>
		</div>
		<div class="detail-aminities">
			<h3>Amenities</h3>
			<ul>
				<li><i class="icon-check2"></i>Balcony</li>
				<li><i class="icon-check2"></i>Basement parking</li>
				<li><i class="icon-check2"></i>BBQ area</li>
				<li><i class="icon-check2"></i>Broadband ready</li>
				<li><i class="icon-check2"></i>Built in wardrobes</li>
				<li><i class="icon-check2"></i>Carpets</li>
				<li><i class="icon-check2"></i>Swimming Pool</li>
				<li><i class="icon-check2"></i>Super Market</li>
			</ul>
		</div>
		<div class="house-description">
			<h3>Penthouse Description</h3>
			<p>OBG proudly present Contemporary Duplex Half Penthouse in The One, West Trunk , Palm Island with Fantastic View of Gulf / Marina Skyline / Dubai Eye Views.</p>
			<br>
			<p>Corner Duplex 5 bedroom <br>
			Indoor area:13234.77 SQF </p>
		</div>
		<div class="month-costs">
			<h3>Monthly Costs</h3>
			<p>Calculations based on average cost per square feet for Apartments. This includes the monthly municipal tax. Use the editable fields to modify calculations appropriately.</p>
		</div>
	</div>
	<div class="col-md-4 margeted-by">
		<div class="property-from">
			<p>Buy this property from only</p>
			<span>AED 431,584 / month</span>
			<a href="#">Get Home Finance Now</a>
		</div>
		<div class="marketed-by">
			<h3>Marketed By</h3>
			<div class="list-name">
				<span>Name:</span><span>Residence West Crescent</span>
			</div>
			<div class="list-name">
				<span>License:</span><span>985213</span>
			</div>
			<div class="list-name">
				<span>Agency:</span><span>OBG Real Estate Broker</span>
			</div>
			<p>View all properties by OBG Real Estate Broker</p>
			<a href="#" class="call-back"><i class="icon-phone-call"></i>Call</a>
			<a href="#" class="call-back"><i class="icon-phone-incoming"></i>Callback</a>
			<a href="#" class="call-back"><i class="icon-mail2"></i>Email</a>
		</div>
		<div class="req-details">
			<h3>Request for more details</h3>
			<form>
				<label><input type="text" name="" placeholder="Full Name"></label>
				<label><input type="text" name="" placeholder="Email"></label>
				<label><input type="text" name="" placeholder="Your Phone"></label>
				<label>Your Message<textarea rows="5"></textarea></label>
				<input type="submit" name="" value="send">
			</form>
		</div>
		<div class="mortage-calci">
			<h3>Mortgage Calculator</h3>
			<ul>
				<li>
					<h4>Purchase Price</h4>
					<span>AED 93,330,000</span>
				</li>
				<li>
					<h4>Down Payment (AED)</h4>
					<input type="text" name="" value="23569874">
				</li>
				<li>
					<h4>Term (Years)</h4>
					<input type="text" name="" value="10">
				</li>
				<li>
					<h4>Profit (% p.a.)</h4>
					<input type="text" name="" value="4.2">
				</li>
				<input type="submit" name="" value="Get Home Finance Now">
			</ul>
		</div>
	</div>
	<div class="back-bg similar-areana">
		<div class="home-section container">
			<h2>Similar Properties</h2>
			<div class="resiential-properties">
				<?php for($i = 0; $i < 3; $i ++): ?>
				<div class="each-props">
					<figure><img src="dist/images/house.jpg"></figure>
					<h3><a href="#">NY Simplex 5 Bed W Residence West Crescent</a></h3>
					<h5>Penthouse for Sale</h5>
					<ul>
						<li><i class="icon-area-graph"></i>5 Beds</li>
						<li><i class="icon-area-graph"></i>6 Baths</li>
						<li><i class="icon-area-graph"></i>10642 Sqft</li>
					</ul>
					<span>The Alef Residences, The Palm Jumeirah, Dubai</span>
					<a href="#" class="call-back"><i class="icon-phone-call"></i>Call</a>
					<a href="#" class="call-back"><i class="icon-phone-incoming"></i>Callback</a>
					<a href="#" class="call-back"><i class="icon-mail2"></i>Email</a>
					<hr>
					<small>$ 50,000,000</small><main>Property Id #12345679890</main>
				</div>
				<?php endfor; ?> 
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>