<?php include 'header.php'; ?>
<div class="home-banner container-fluid">
	<img src="dist/images/banner.jpg">
</div>
<div class="banner-section container">
	<h1>Discover more</h1>
	<h2>Find your perfect property & neighbourhood</h2>
	<div class="search-result">
		<select class="form-control" id="select_type">
			<option>Buy</option>
			<option>Rent</option>
			<option>Short Term Rent</option>
			<option>Commercial Short Term Rent</option>
			<option>Commercial Buy</option>
			<option>Commercial Rent</option>
	    </select>
	    <input type="text" name="" placeholder="Areas, Cities and buildings..">
	    <button type="button">Search</button>
	</div>
	<div class="filter-search">
		<div class="collapse" id="adv_search">
			<?php for($i = 0; $i < 10; $i ++): ?>
		    <select class="form-control" id="prop_type">
				<option>Buy</option>
				<option>Rent</option>
				<option>Short Term Rent</option>
				<option>Commercial Short Term Rent</option>
				<option>Commercial Buy</option>
				<option>Commercial Rent</option>
		    </select>
		    <?php endfor; ?> 
		</div>
		<button class="btn" type="button" data-toggle="collapse" data-target="#adv_search" aria-expanded="false" aria-controls="collapseExample">+ Advance Search</button>
		<button>Clear Filter</button>	
		<img src="dist/images/advertise.png">
	</div>
</div>
<div class="back-bg">
	<div class="home-section container">
		<h2>Featured Residential Properties for Sale</h2>
		<div class="resiential-properties col-md-9">
			<?php for($i = 0; $i < 2; $i ++): ?>
			<div class="each-props">
				<figure><img src="dist/images/house.jpg"></figure>
				<h3><a href="#">NY Simplex 5 Bed W Residence West Crescent</a></h3>
				<h5>Penthouse for Sale</h5>
				<ul>
					<li><i class="icon-area-graph"></i>5 Beds</li>
					<li><i class="icon-area-graph"></i>6 Baths</li>
					<li><i class="icon-area-graph"></i>10642 Sqft</li>
				</ul>
				<span>The Alef Residences, The Palm Jumeirah, Dubai</span>
				<a href="#" class="call-back"><i class="icon-phone-call"></i>Call</a>
				<a href="#" class="call-back"><i class="icon-phone-incoming"></i>Callback</a>
				<a href="#" class="call-back"><i class="icon-mail2"></i>Email</a>
				<hr>
				<small>$ 50,000,000</small><main>Property Id #12345679890</main>
			</div>
			<?php endfor; ?> 
		</div>
		<div class="properties-advertise col-md-3">
			<figure><img src="dist/images/print-ad.jpg"></figure>
			<figure><img src="dist/images/print-ad.jpg"></figure>
		</div>
	</div>
	<div class="lets-guide container">
		<h2>Let us guide you</h2>
		<ul>
			<li>
				<figure><img src="dist/images/real-estate.jpg"></figure>
				<h3><a href="#">Top Villas Under AED 1 Million In Cambodia!</a></h3>
			</li>
			<li>
				<figure><img src="dist/images/real-estate.jpg"></figure>
				<h3><a href="#">Best Areas To Buy Villas In Cambodia</a></h3>
			</li>
			<li>
				<figure><img src="dist/images/real-estate.jpg"></figure>
				<h3><a href="#">Checkout Our New Project!!</a></h3>
			</li>
		</ul>
	</div>
</div>
<?php include 'footer.php'; ?>