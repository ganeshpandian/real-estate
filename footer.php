<footer id="prop-deta">
	<div class="up-date container">
		<span class="pull-right">
			<label>
				<input type="text" name="" placeholder="Email">
				<i class="icon-send"></i>
			</label>
			<button>Subscribe</button>
		</span>
		<h3>Keep up to date</h3>
		<p>Leave us your email address and we will be sure to keep you informed about latest properties.</p>
	</div>
	<div class="footer-section container">
		<div class="keep-touch logo-foot">
			<h3>Keep In Touch</h3>
			<img src="dist/images/new-logo.png">
			<a href="#"><i class="icon-mail2"></i>Versace@gmail.com</a>
			<ul>
				<li><a href="#"><i class="icon-facebook"></i></a></li>
				<li><a href="#"><i class="icon-twitter2"></i></a></li>
				<li><a href="#"><i class="icon-instagram2"></i></a></li>
				<li><a href="#"><i class="icon-google"></i></a></li>
			</ul>
			<a href="#" class="app-store"><img src="dist/images/App-store.png"></a>
			<a href="#" class="app-store"><img src="dist/images/google-store.png"></a>
		</div>
		<div class="keep-touch">
			<h3>Popular Searches</h3>
			<a href="#">Villa for rent in Cambodia</a>
			<a href="#">Apartments for rent in Cambodia</a>
			<a href="#">Apartments for sale in Cambodia</a>
			<a href="#">Villa for sale in Cambodia</a>
		</div>
		<div class="keep-touch">
			<h3>Popular Areas</h3>
			<a href="#">Villa for rent in Cambodia</a>
			<a href="#">Apartments for rent in Cambodia</a>
			<a href="#">Apartments for sale in Cambodia</a>
			<a href="#">Villa for rent in Cambodia</a>
			<a href="#">Apartments for rent in Cambodia</a>
			<a href="#">Apartments for sale in Cambodia</a>
			<a href="#">Villa for sale in Cambodia</a>
		</div>
		<div class="keep-touch">
			<h3>Trending Properties</h3>
			<a href="#">The Views</a>
			<a href="#">Arjan</a>
			<a href="#">Jumeirah Beach Residences (JBR)</a>
			<a href="#">Meadows</a>
			<a href="#">Cambodia Creek Harbour (The Lagoons)</a>
			<a href="#">Arabian Ranches</a>
		</div>
	</div>
	<div class="copy-right">
		<span class="pull-left">
			<a href="#">TERMS AND CONDITIONS</a>  
			<a href="#">PRIVACY POLICY</a>  
			<a href="#">FOR BROKERS</a>
		</span>
		<span class="pull-right">
			<a href="#">© VERSACE.COM</a>
		</span>
	</div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="dist/scripts/main.js"></script>
<script src="dist/scripts/skrollr.min.js"></script>
<script src="dist/scripts/wow.min.js"></script>
</body>
</html>