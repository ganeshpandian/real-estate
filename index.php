<?php include 'header.php'; ?>

<div class="container">
	<h1>Real Estate pages</h1>
	<ul>
		<li>
			<h2>FrontEnd</h2>
			<ul>
				<li><a href="home.php" target="_blank">Home Page</a></li>
				<li><a href="search-list.php" target="_blank">Search Listing Page</a></li>
				<li><a href="detail-page.php" target="_blank">Detail Page</a></li>
			</ul>
		</li>
		<li>
			<h2>Backend</h2>
			<ul>
				<li><a href="userdashboard.php" target="_blank">User Dashboard</a></li>
				<li><a href="addnew-property.php" target="_blank">Add New Property Page</a></li>
			</ul>
		</li>
	</ul>
</div>

<?php include 'footer.php'; ?>