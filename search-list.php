<?php include 'header.php'; ?>
<div class="adding-banner">
	<div class="banner-section container listing-area">
		<div class="search-result">
			<select class="form-control" id="select_type">
				<option>Buy</option>
				<option>Rent</option>
				<option>Short Term Rent</option>
				<option>Commercial Short Term Rent</option>
				<option>Commercial Buy</option>
				<option>Commercial Rent</option>
		    </select>
		    <input type="text" name="" placeholder="Areas, Cities and buildings..">
		    <button type="button">Search</button>
		</div>
		<div class="filter-search">
			<div class="collapse" id="adv_search">
				<?php for($i = 0; $i < 10; $i ++): ?>
			    <select class="form-control" id="prop_type">
					<option>Buy</option>
					<option>Rent</option>
					<option>Short Term Rent</option>
					<option>Commercial Short Term Rent</option>
					<option>Commercial Buy</option>
					<option>Commercial Rent</option>
			    </select>
			    <?php endfor; ?> 
			</div>
			<button class="btn" type="button" data-toggle="collapse" data-target="#adv_search" aria-expanded="false" aria-controls="collapseExample">+ Advance Search</button>
			<button>Clear Filter</button>	
		</div>
	</div>
</div>
<div class="sorting-results">
	<div class="resident-property container">
		<div class="dropdown furnish-search">
		  <button class="btn btn-secondary dropdown-toggle" type="button" id="sort_resu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    <i class="fa fa-sort" aria-hidden="true"></i>Sort Results
		  </button>
		  <div class="dropdown-menu" aria-labelledby="sort_resu">
		    <a class="dropdown-item" href="#"><i class="icon-arrow-down"></i>low to high</a>
		    <a class="dropdown-item" href="#"><i class="icon-arrow-up"></i>high to low</a>
		    <a class="dropdown-item" href="#"><i class="icon-arrow-down"></i>low to high</a>
		    <a class="dropdown-item" href="#"><i class="icon-arrow-up"></i>high to low</a>
		    <a class="dropdown-item" href="#"><i class="icon-arrow-down"></i>low to high</a>
		    <a class="dropdown-item" href="#"><i class="icon-arrow-up"></i>high to low</a>
		  </div>
		</div>
		<div class="map-view furnish-search">
			<button class="list-style"><i class="icon-map"></i>Map View</button>
			<button class="grid-style" style="display: none"><i class="icon-list"></i>List View</button>
		</div>
		<div class="view-types furnish-search">
			<button class="list-style"><i class="icon-list"></i>List View</button>
			<button class="grid-style" style="display: none"><i class="icon-grid2"></i>Grid View</button>
		</div>
	</div>
</div>
<div class="back-bg listing-space">
	<div class="home-section container">
		<h2>Residential Properties for Sale in Phnom Penh</h2>
		<div class="resiential-properties col-md-9">
			<?php for($i = 0; $i < 10; $i ++): ?>
			<div class="each-props">
				<figure><img src="dist/images/house.jpg"></figure>
				<div class="txt-right">
					<h3><a href="#">NY Simplex 5 Bed W Residence West Crescent</a></h3>
					<h5>Penthouse for Sale</h5>
					<ul>
						<li><i class="icon-area-graph"></i>5 Beds</li>
						<li><i class="icon-area-graph"></i>6 Baths</li>
						<li><i class="icon-area-graph"></i>10642 Sqft</li>
					</ul>
					<span>The Alef Residences, The Palm Jumeirah, Dubai</span>
					<a href="#" class="call-back"><i class="icon-phone-call"></i>Call</a>
					<a href="#" class="call-back"><i class="icon-phone-incoming"></i>Callback</a>
					<a href="#" class="call-back"><i class="icon-mail2"></i>Email</a>
					<hr>
					<small>$ 50,000,000</small><main>Property Id #12345679890</main>
				</div>
			</div>
			<?php endfor; ?> 
		</div>
		<div class="properties-advertise col-md-3">
			<div class="right-add">
				<figure><img src="dist/images/print-ad.jpg"></figure>
				<figure><img src="dist/images/print-ad.jpg"></figure>
				<figure><img src="dist/images/print-ad.jpg"></figure>
				<figure><img src="dist/images/print-ad.jpg"></figure>
				<figure><img src="dist/images/print-ad.jpg"></figure>
				<figure><img src="dist/images/print-ad.jpg"></figure>
			</div>
			<div class="right-map" style="display: none">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.987643314419!2d104.84228751530667!3d11.552743291797572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31094fd553400b85%3A0xd5336e32f60085ec!2sPhnom+Penh+International+Airport!5e0!3m2!1sen!2skh!4v1549337440828" width="100%" height="1000" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>