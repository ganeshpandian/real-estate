jQuery(document).ready(function ($) {
  $('.owl-carousel').owlCarousel({
      loop:false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:false,
              loop:true,
              dots: true,
              autoplay: true
          },
          520:{
              items:1,
              nav:false,
              loop:false
          },
          1023:{
              items:1,
              nav:true,
              dots: false,
              loop:false
          }
      }
  });
});
jQuery('img.svg').each(function(){
  var $img = jQuery(this);
  var imgID = $img.attr('id');
  var imgClass = $img.attr('class');
  var imgURL = $img.attr('src');

  jQuery.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');

      // Add replaced image's ID to the new SVG
      if(typeof imgID !== 'undefined') {
          $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if(typeof imgClass !== 'undefined') {
          $svg = $svg.attr('class', imgClass+' replaced-svg');
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      // Replace image with new SVG
      $img.replaceWith($svg);

  }, 'xml');
});
$('.map-view button').click(function(){
    $('.resiential-properties').toggleClass("change-width");
    $('.properties-advertise').toggleClass("increase-width");
    $('.resiential-properties').removeClass("change-view");
    $('.properties-advertise').removeClass("increase-view");
    $('.map-view').toggleClass("change-name");
});
$('.view-types button').click(function(){
    $('.resiential-properties').toggleClass("change-view");
    $('.properties-advertise').toggleClass("increase-view");
    $('.resiential-properties').removeClass("change-width");
    $('.properties-advertise').removeClass("increase-width");
    $('.view-types').toggleClass("change-name");
});

$("span#password-eye i").click(function () {
  if ($("#password").attr("type")=="password") {
      $("#password").attr("type", "text");
  }
  else{
      $("#password").attr("type", "password");
  }
});
$("span#password-eye i").click(function () {
  $('span#password-eye i').toggleClass('icon-eye');
});

$("span#password-eyes i").click(function () {
  if ($("#conpass").attr("type")=="password") {
      $("#conpass").attr("type", "text");
  }
  else{
      $("#conpass").attr("type", "password");
  }
});
$("span#password-eyes i").click(function () {
  $('span#password-eyes i').toggleClass('icon-eye');
});

$("span#password-yes i").click(function () {
  if ($("#agent_pass").attr("type")=="password") {
      $("#agent_pass").attr("type", "text");
  }
  else{
      $("#agent_pass").attr("type", "password");
  }
});
$("span#password-yes i").click(function () {
  $('span#password-yes i').toggleClass('icon-eye');
});





