<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="dist/images/new-logo.png">
	
	<link rel="stylesheet" href="dist/styles/vendor.css?v=0.0.1">
	<link rel="stylesheet" href="dist/scripts/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="dist/styles/style.css?v=0.0.1">
	<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	
	<script src="dist/scripts/plugins/vendor.js" type="text/javascript"></script>
	<script src="dist/scripts/plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="front-display">
	<div class="head-parts container">
		<header class="main-header"> 
			<a class="logo" href="/">
				<img src="./dist/images/new-logo.png" alt="real-estate" />
			</a>
			<div class="page-menu">
				<ul class="social-login">
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-twitter2"></i></a></li>
					<li><a href="#"><i class="icon-instagram2"></i></a></li>
					<li><a href="#"><i class="icon-google"></i></a></li>
					<li>
						<a href="#"  data-toggle="modal" data-target="#agent_pop">Agent Login</a>
					</li>
					<li><a href="#" data-toggle="modal" data-target="#login_pop">Sign In</a></li>
				</ul>
				<ul>
					<li><a href="#">Buy</a></li>
					<li><a href="#">Rent</a></li>
					<li><a href="#">New Project</a></li>
					<li><a href="#">Commercial</a></li>
					<li><a href="#">Budget Search</a></li>
					<li><a href="#">Agent Finder</a></li>
					<li><a href="#">Create Alert</a></li>
				</ul>
			</div>
		</header>
	</div>


	
<div class="modal fade" id="login_pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<div class="col-md-5 click-log">
		<h2>Real Estate Login</h2>
		<p>Don't you have an account? <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#register_pop">Create your account</a>, it takes less than a minute.</p> 
		<div id="login_mail">
			<form>
			<div class="form-group">
				<input type="text" id="mailing" class="form-control" required>
				<span class="floating-label"><b>*</b>Enter your email</span>
			</div>
			<div class="form-group">
				<input type="password" id="password" class="form-control" required>
				<span class="floating-label">Password</span>
				<span id="password-eye"> <i class="fa fa-eye-slash"></i></span>
			</div>
			<div class="button-submit">
				<span class="col-md-6">
					<input type="submit" name="" value="Log In">
				</span>
				<span class="col-md-6 text-right">
					<a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#forgot_password">Forgot Password?</a>
				</span>
			</div>
		</form> 		
		</div>		
  	</div>
  	<div class="col-md-6 add-sec phone-hide">
  		<img src="dist/images/building.jpg">
  		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <i class="icon-cross"></i>
	    </button>
  	</div>
  </div>
</div>


<div class="modal fade" id="register_pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<div class="col-md-5 click-log">
		<h2>Real Estate Register</h2>
		<form>
			<div class="form-group col-md-6">
				<input type="text" id="fname" class="form-control" required>
				<span class="floating-label">First Name</span>
			</div>
			<div class="form-group col-md-6">
				<input type="text" id="lname" class="form-control" required>
				<span class="floating-label">Last Name</span>
			</div>
			<!-- <div class="form-group col-md-12">
				<input type="text" id="mails" class="form-control" required>
				<span class="floating-label"><b>*</b>Enter your email</span>
			</div> -->
			<div class="form-group col-md-12 register-mobile">
				<div class="form-group col-md-3">
					<input type="text" class="form-control" value="+855">
				</div>
				<div class="form-group col-md-9">
					<input type="text" id="mails" class="form-control" required>
					<span class="floating-label"><b>*</b>Enter your Mobile Number</span>
				</div>
			</div>
			<div class="passy-sec">
				<div class="form-group col-md-6">
					<input type="password" id="regpass" class="form-control" required>
					<span class="floating-label"><b>*</b>Password</span>
				</div>
				<div class="form-group col-md-6">
					<input type="password" id="conpass" class="form-control" required>
					<span class="floating-label"><b>*</b>Confirm Password</span>
					<span id="password-eyes"> <i class="fa fa-eye-slash"></i></span>
				</div>
				<p>Password must be more than 8 characters,capitals, Special character.</p>
			</div>
			<div class="button-submit">
				<span class="col-md-8">
					<input type="submit" name="" value="Register">
				</span>
			</div>
		</form> 
		<div class="back-to-login">
			<p>Already have an account <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#login_pop">Log In</a></p>
		</div>
	</div>
	<div class="col-md-6 add-sec phone-hide">
  		<img src="dist/images/building.jpg">
  		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <i class="icon-cross"></i>
	    </button>
  	</div>
  </div>
</div>




<div class="modal fade" id="agent_pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<div class="col-md-5 click-log">
		<h2>Real Estate Register</h2>
		<form>
			<div class="form-group col-md-6">
				<input type="text" id="fname" class="form-control" required>
				<span class="floating-label" for="fname">First Name</span>
			</div>
			<div class="form-group col-md-6">
				<input type="text" id="lname" class="form-control" required>
				<span class="floating-label" for="lname">Last Name</span>
			</div>
			<!-- <div class="form-group col-md-12 or-mobile">
				<input type="text" id="mails" class="form-control" required>
				<span class="floating-label" for="mails"><b>*</b>Enter your email</span>
			</div> -->
			<div class="form-group col-md-12 register-mobile">
				<div class="form-group col-md-3">
					<input type="text" class="form-control" value="+855">
				</div>
				<div class="form-group col-md-9">
					<input type="text" id="mails" class="form-control" required>
					<span class="floating-label"><b>*</b>Enter your Mobile Number</span>
				</div>
			</div>
			<div class="passy-sec">
				<div class="form-group col-md-6">
					<input type="password" id="regpass" class="form-control" required>
					<span class="floating-label" for="regpass"><b>*</b>Password</span>
				</div>
				<div class="form-group col-md-6">
					<input type="password" id="agent_pass" class="form-control" required>
					<span class="floating-label" for="agent_pass"><b>*</b>Confirm Password</span>
					<span id="password-yes"> <i class="fa fa-eye-slash"></i></span>
				</div>
				<p>Password must be more than 8 characters,capitals, Special character.</p>
			</div>
			<div class="shoe-user">
				<div class="col-md-7 agent-area">
					<div class="agent-info">
						<input type="text" id="compname" class="form-control" required>
						<span class="floating-label" for="compname">Company Name</span>
					</div>
					<div class="agent-info">
						<input type="text" id="compaddr" class="form-control" required>
						<span class="floating-label" for="compaddr">Company Address</span>
					</div>
					<div class="agent-info">
						<input type="text" id="compnumber" class="form-control" required>
						<span class="floating-label" for="compnumber">Company License Number</span>
					</div>
				</div>
				<div class="col-md-5 agent_upload">
				    <label>Bussiness License</label>
				    <label class="bussiness-field" for="bussiness_licience">
				        <figure><i class="icon-upload-cloud"></i></figure>
				        <h3>Click here to upload</h3>
				        <span class="files" id="files"></span>
				        <input name="bussiness_licience" id="bussiness_licience" type="file" multiple="">
				    </label>
				</div>
			</div>
			<div class="button-submit">
				<span class="col-md-8">
					<input type="submit" name="" value="Register">
				</span>
			</div>
		</form> 
		<div class="back-to-login">
			<p>Already have an account <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#login_pop">Log In</a></p>
		</div>
	</div>
	<div class="col-md-6 add-sec phone-hide">
  		<img src="dist/images/building.jpg">
  		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <i class="icon-cross"></i>
	    </button>
  	</div>
  </div>
</div>
	



